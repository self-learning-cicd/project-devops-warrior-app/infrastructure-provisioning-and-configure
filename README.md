# Provisioning and Configuring deployment automation

In this step, the repository will handle provisioning infrastructure in Amazon AWS and configuring it with automation tool

# Tool will be use
1. Terraform (Provisioning tool) as IaaC (Infrastructure as a Code)
2. Ansible (Configuration Management tool) via SSH to provisioned infrastructure
3. Gitlab CI/CD (Automation execution tool in above)

# Step 1. Get credential from AWS
export AWS_ACCESS_KEY_ID=access key from AWS
export AWS_SECRET_ACCESS_KEY=secret key from AWS

# Step 2. Preparing Provisioning Script
```
cd terraform
terraform init #init terraform
terraform plan -out tfplan #plan terraform
terraform apply "tfplan" #apply
```
If u have wrong plan or stuck , u can delete the resource created before with this command
```
terraform plan -destroy -out tfplan
terraform apply "tfplan"

```
# Step 3. Preparing Configuring script


# Step 4. Preparing CI/CD script
In this provisioning, terraform will be use state backend provided by gitlab, just add this `backend http` type in providers.tf
```
terraform {
  backend "http" {
  }
}
``` 


