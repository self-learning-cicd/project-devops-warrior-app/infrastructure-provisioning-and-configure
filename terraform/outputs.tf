output "masterinstance" {
  value = aws_instance.master_node.public_ip
}

output "worker1instance" {
  value = aws_instance.worker_node_01.public_ip
}
