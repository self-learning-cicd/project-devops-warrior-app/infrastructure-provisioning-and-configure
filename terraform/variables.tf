variable "awsprops" {
  type = map(any)
  default = {
    region       = "ap-southeast-1"        #default region (Singapore)
    secgroupname = "IAC-Sec-Group"         #security group used, this will be match with all vpc and subnet
    ami          = "ami-07651f0c4c315a529" #change to use Ubuntu 22.04 LTS
    itype        = "t2.micro"              #use default instance type t1.micro
    publicip     = true                    #use public ip by default
    keyname      = "devops-warrior-key"    #secret name for connecting to Instance
  }
}
