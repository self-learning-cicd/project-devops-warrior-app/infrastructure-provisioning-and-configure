resource "aws_vpc" "devops-warrior-vpc" {
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true #gives you an internal domain name
  enable_dns_hostnames = true #gives you an internal host name
  instance_tenancy     = "default"

  tags = {
    Name = "dev-vpc"
  }
}

resource "aws_subnet" "dev-devops-warrior-subnet" {
  vpc_id                  = aws_vpc.devops-warrior-vpc.id
  cidr_block              = "192.168.0.0/20"
  map_public_ip_on_launch = true //it makes this a public subnet
  availability_zone       = "ap-southeast-1a"
  tags = {
    Name = "dev-devops-warrior-subnet"
  }
}

resource "aws_internet_gateway" "dev-igw" {
  vpc_id = aws_vpc.devops-warrior-vpc.id
  tags = {
    Name = "dev-igw"
  }
}

//create routing table instance
resource "aws_route_table" "dev-public-crt" {
  vpc_id = aws_vpc.devops-warrior-vpc.id
  route {
    //associated subnet can reach everywhere
    cidr_block = "0.0.0.0/0"
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.dev-igw.id
  }
  tags = {
    Name = "dev-public-crt"
  }
}

//associate crt to subnet
resource "aws_route_table_association" "dev-crt-devops-warrior-subnet-1" {
  subnet_id      = aws_subnet.dev-devops-warrior-subnet.id
  route_table_id = aws_route_table.dev-public-crt.id
}

//create security group to allow several access port
resource "aws_security_group" "project-devops-warrior-sg" {
  name        = lookup(var.awsprops, "secgroupname")
  description = lookup(var.awsprops, "secgroupname")
  vpc_id      = aws_vpc.devops-warrior-vpc.id

  // To Allow SSH Transport
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 443 Transport
  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 6443 Kube API Server
  ingress {
    from_port   = 6443
    protocol    = "tcp"
    to_port     = 6443
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 10250 Kubelet metric
  ingress {
    from_port   = 10250
    protocol    = "tcp"
    to_port     = 10250
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow connection all dst
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

// provisioning load balancer type network for in this application
resource "aws_lb" "dev-devops-warrior-lb" {
  name               = "dev-devops-warrior-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.dev-devops-warrior-subnet.id]

  enable_deletion_protection = false
  tags = {
    Environment = "DEV"
  }
}

resource "aws_lb_listener" "dev-devops-warrior-frontend" {
  load_balancer_arn = aws_lb.dev-devops-warrior-lb.arn
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.dev-devops-warrior-tg.arn
  }
}

resource "aws_lb_target_group" "dev-devops-warrior-tg" {
  name     = "dev-devops-warrior-tg"
  port     = 80
  protocol = "TCP"
  vpc_id   = aws_vpc.devops-warrior-vpc.id
}

//register instance to target group
resource "aws_lb_target_group_attachment" "dev-devops-warrior-master-tga" {
  target_group_arn = aws_lb_target_group.dev-devops-warrior-tg.arn
  target_id        = aws_instance.master_node.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "dev-devops-warrior-worker01-tga" {
  target_group_arn = aws_lb_target_group.dev-devops-warrior-tg.arn
  target_id        = aws_instance.worker_node_01.id
  port             = 80
}


resource "aws_instance" "master_node" {
  ami                         = lookup(var.awsprops, "ami") #ami (image) for booting OS
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.dev-devops-warrior-subnet.id
  associate_public_ip_address = lookup(var.awsprops, "publicip")
  key_name                    = lookup(var.awsprops, "keyname")
  availability_zone           = aws_subnet.dev-devops-warrior-subnet.availability_zone

  vpc_security_group_ids = [
    aws_security_group.project-devops-warrior-sg.id
  ]

  root_block_device {
    delete_on_termination = true
    volume_size           = 15
    volume_type           = "gp2"
  }

  tags = {
    Name        = "CONTROL_NODE"
    Environment = "DEV"
    OS          = "UBUNTU"
    Managed     = "IAC"
  }

  depends_on = [aws_security_group.project-devops-warrior-sg]
}

resource "aws_instance" "worker_node_01" {
  ami                         = lookup(var.awsprops, "ami") #ami (image) for booting OS
  instance_type               = lookup(var.awsprops, "itype")
  subnet_id                   = aws_subnet.dev-devops-warrior-subnet.id
  associate_public_ip_address = lookup(var.awsprops, "publicip")
  key_name                    = lookup(var.awsprops, "keyname")
  availability_zone           = aws_subnet.dev-devops-warrior-subnet.availability_zone

  vpc_security_group_ids = [
    aws_security_group.project-devops-warrior-sg.id
  ]

  root_block_device {
    delete_on_termination = true
    volume_size           = 10
    volume_type           = "gp2"
  }

  tags = {
    Name        = "WORKER01_NODE"
    Environment = "DEV"
    OS          = "UBUNTU"
    Managed     = "IAC"
  }

  depends_on = [aws_security_group.project-devops-warrior-sg]

}






